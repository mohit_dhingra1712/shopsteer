package com.tad.framework.db;

import android.app.Application;
import android.util.Log;

/**
 *
 * This initilizes database helper and stores so that this can be used by other activities when ever required
 *
 */
public class MyApplication extends Application {

	public static final String APP_NAME = "My Appname";

	private DataBaseHelper dataHelper;

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(APP_NAME, "APPLICATION onCreate");
		this.dataHelper = new DataBaseHelper(this);
	}

	@Override
	public void onTerminate() {
		Log.d(APP_NAME, "APPLICATION onTerminate");
		super.onTerminate();
	}

	public DataBaseHelper getDataHelper() {
		return this.dataHelper;
	}

	public void setDataHelper(DataBaseHelper dataHelper) {
		this.dataHelper = dataHelper;
	}
}
