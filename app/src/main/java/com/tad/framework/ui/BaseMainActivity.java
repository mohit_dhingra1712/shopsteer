/**
 *
 */
package com.tad.framework.ui;



import android.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;

//PLEASE TRY TO KEEP IT LIGHT.......
/**
 * It provides basic functionality for a normal Screens.. Extends it to modify base.
 * Since 1.0
 */
public abstract class BaseMainActivity extends Activity implements IScreen {

	protected ProgressDialog mProgressDialog;
	private String mProgressDialogBodyText = "Loading. Please wait...";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

	}

	/*
	 * (non-Javadoc)
	 * @see com.test.ui.IScreen#setScreenData(java.lang.Object, int, long)
	 */
	public void setScreenData(Object screenData, int event, long time)
	{

	}


	public void startSppiner(View view,String titleTxt,String bodyText,boolean isCancelable) {
		if(null == mProgressDialog){
			mProgressDialog = ProgressDialog.show(this, titleTxt, bodyText, true);
			mProgressDialog.setCancelable(isCancelable);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL|ProgressDialog.STYLE_SPINNER);
			if(null!=view)
				mProgressDialog.setView(view);
		}
		if(!mProgressDialog.isShowing())
			mProgressDialog.show();
	}
	public void startSppiner(String titleTxt,String bodyText,boolean isCancelable) {
		startSppiner(null,titleTxt,bodyText,isCancelable);
	}
	public void startSppiner(boolean isCancelable) {
		startSppiner("",mProgressDialogBodyText,isCancelable);
	}
	public void startSppiner() {
		startSppiner(false);
	}

	public void stopSppiner() {
		if(null != mProgressDialog && mProgressDialog.isShowing())
			mProgressDialog.dismiss();

	}


	public void setProgressDialogBodyText(String progressDialogBodyText) {
		mProgressDialogBodyText = progressDialogBodyText;
	}

   public Activity getMyActivityReference(){
	   return this;//TODO IS THIS RIGHT
   }

   public void reset(){

   }
   
   protected int getActionBarSize() {
       TypedValue typedValue = new TypedValue();
       int[] textSizeAttr = new int[]{R.attr.actionBarSize};
       int indexOfAttrTextSize = 0;
       TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
       int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
       a.recycle();
       return actionBarSize;
   }
}
