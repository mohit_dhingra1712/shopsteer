package com.tad.framework.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;



public class BaseMainCompatActivity extends AppCompatActivity implements IScreen {


    protected ProgressDialog mProgressDialog;
    private String mProgressDialogBodyText = "Loading. Please wait...";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
    }

    @Override
    public void setScreenData(Object screenData, int event, long time) {

    }

    @Override
    public Activity getMyActivityReference() {
        return this;
    }
    public void startSppiner(View view, String titleTxt, String bodyText, boolean isCancelable) {
        if(null == mProgressDialog){
            mProgressDialog = ProgressDialog.show(this, titleTxt, bodyText, true);
            mProgressDialog.setCancelable(isCancelable);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL|ProgressDialog.STYLE_SPINNER);
            if(null!=view)
                mProgressDialog.setView(view);
        }
        if(!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    public void startSppiner(String titleTxt,String bodyText,boolean isCancelable) {
        startSppiner(null,titleTxt,bodyText,isCancelable);
    }
    public void startSppiner(boolean isCancelable) {
        startSppiner("", mProgressDialogBodyText, isCancelable);
    }
    @Override
    public void startSppiner() {
        startSppiner(false);

    }

    @Override
    public void stopSppiner() {

        if(null != mProgressDialog && mProgressDialog.isShowing())
        mProgressDialog.dismiss();
    }
}
