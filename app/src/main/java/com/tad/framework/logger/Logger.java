package com.tad.framework.logger;

import android.util.Log;

public class Logger{

	public static final short LEVEL_VERBOSE = 0;
	public static final short LEVEL_DEBUG =1;
	public static final short LEVEL_INFO =2;
	public static final short LEVEL_WARNNING =3;
	public static final short LEVEL_ERROR =4;

	public static void easyLog(short level, String fromClass, String message){

		switch(level){
		case LEVEL_VERBOSE:
			Log.v(fromClass, message);
			break;
		case LEVEL_DEBUG:
			Log.d(fromClass, message);
			break;
		case LEVEL_INFO:
			Log.i(fromClass, message);
			break;
		case LEVEL_WARNNING:
			Log.w(fromClass, message);
			break;
		case LEVEL_ERROR:
			Log.e(fromClass, message);
		default:
			Log.v(fromClass, message);

		}
	}
}