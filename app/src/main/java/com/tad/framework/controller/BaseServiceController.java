/**
 * BaseServiceController.java
 * DbyDx Software ltd. @2011 - 2011+1
 * Confidential and proprietary.
 */
package com.tad.framework.controller;

import com.tad.framework.ui.IActionController;

/**
 *
 */
public abstract class BaseServiceController implements IServiceController
{
        protected IActionController mScreen;
        protected int mEventType;


        /**
         * Constructor.
         * @param screen instance of a screen to response.
         * @param eventType Every screen has multiple events IllegalArgumentException if Screen instance is null.
         */
        public BaseServiceController(IActionController screen, int eventType) {
                super();
                this.mScreen = screen;
                this.mEventType = eventType;
        }

        /**
         * @return controller
         */
        public IActionController getController() {
                return mScreen;
        }

        /**
         * @return requested event
         */
        public int getEventType() {
                return mEventType;
        }
}
