/**
 * IServiceController.java
 * DbyDx Software ltd. @2010-2011
 * Confidential and proprietary.
 */
package com.tad.framework.controller;

/**
 *
 */
public interface IServiceController {

	/**
	 * Initialize request object here.
	 */
	public void initService();

	/**
	 * Raise your request using Connector.
	 */

	public void requestService(Object object);

	/**
	 * make response for UI.
	 */

	public void responseService(Object object);

}
