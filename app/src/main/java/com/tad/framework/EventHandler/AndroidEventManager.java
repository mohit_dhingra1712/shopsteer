package com.tad.framework.EventHandler;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;


import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.tad.framework.model.Request;
import com.tad.framework.model.Response;

import permunite.com.shopsteer.RequestConroller;
//import com.tad.framework.network.MyHttpClient;

public class AndroidEventManager extends EventManager
{
    public static final String TAG = AndroidEventManager.class
            .getSimpleName();

	Activity mActivity;

	public AndroidEventManager(Activity activity){
     super();
     mActivity= activity;
	}
        /**
         * Called to handle the event. Default implementation does nothing.
         *
         * @param int action
         * @param int type
         * @param Response
         *            containing
         *            <ul>
         *            <li>byte[] for Binary data</li>
         *            </ul>
         */
        protected void handle(final int action, final int type, final Request payload) {

            try {
                processRequest(payload, action);

            } catch (Exception e) {
                String Message = e.getMessage();
                Log.d("Mohit", Message);
                System.out.print(Message);
                e.printStackTrace(new PrintStream(System.out));
                /*Response res = new Response();
                res.setErrorCode(Response.EXCEPTION);
                res.setErrorText(e.getMessage());
                res.setException(e);
                raise(action, type, res);*/
            }
        }


        /**
         *
         * @param payload
         * @return
         * @throws Exception
         */
        private void processRequest(final Request payload ,final int action) throws Exception {

            int requestType = payload.getRequestType();

            switch (requestType) {
                case Request.TYPE_GET:
                {
                    StringRequest strReq = new StringRequest(com.android.volley.Request.Method.GET,
                            payload.getUri(), new com.android.volley.Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.d("Mohit", response.toString());
                            Response responsemodel = new Response();
                            responsemodel.setResponseText(response.toString());
                            raise(action, payload.getRequestType(), responsemodel);

                        }
                    }, new com.android.volley.Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d(TAG, "Error: " + error.getMessage());
                            Response res = new Response();
                            res.setErrorCode(Response.EXCEPTION);
                            res.setErrorText(error.getMessage());
                           // res.setException(e);
                            raise(action, payload.getRequestType(), res);

                        }


                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            final HashMap properties = payload.getProperties();
                            return properties;
                        }
                    };

                    RequestConroller.getInstance(mActivity).addToRequestQueue(strReq, "json_obj_req");
                }
                break;

                case Request.TYPE_POST:
                {
                    StringRequest strReq = new StringRequest(com.android.volley.Request.Method.POST,
                            payload.getUri(), new com.android.volley.Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.d("Mohit", response.toString());
                            Response responsemodel = new Response();
                            responsemodel.setResponseText(response.toString());
                            raise(action, payload.getRequestType(), responsemodel);

                        }
                    }, new com.android.volley.Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d(TAG, "Error: " + error.getMessage());
                            Response res = new Response();
                            res.setErrorCode(Response.EXCEPTION);
                            res.setErrorText(error.getMessage());
                            // res.setException(e);
                            raise(action, payload.getRequestType(), res);

                        }


                    }){
                        @Override
                        public String getBodyContentType() {
                          return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return payload.getPayload();
                        }

                        @Override
                        protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                            Log.d("Mohit ","parseNetworkResponse "+response.toString());

                            return super.parseNetworkResponse(response);

                        }

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            final HashMap properties = payload.getProperties();
                            return properties;
                        }
                    };

                    RequestConroller.getInstance(mActivity).addToRequestQueue(strReq, "json_obj_req_post");
                }


                  /*  HashMap<String, String> propertiesPost = payload.getProperties();
                    return client.doPost(payload.getUri(), payload.getPayload(),propertiesPost);*/
                    break;
               /* case Request.TYPE_MULTI_PART:
                    //need to handle
                    break;
                case Request.TYPE_LOG:
                    //need to handle
                    //FileLogger.writeToFile(new String(payload.getPayload()));
                    break;*/

                default:
                    break;
            }


// Adding request to request queue












            /*
                int requestType = payload.getRequestType();
                MyHttpClient client = new MyHttpClient(createHttpClient());
   Log.d("ShopSteer","process request called requestType"+requestType);
                switch (requestType) {
                case Request.TYPE_GET:
                	    HashMap<String, String> properties = payload.getProperties();
                        return client.doGet(payload.getUri(), properties);
                case Request.TYPE_POST:
                        HashMap<String, String> propertiesPost = payload.getProperties();
                        return client.doPost(payload.getUri(), payload.getPayload(),propertiesPost);
                case Request.TYPE_MULTI_PART:
                     //need to handle
                     break;
                case Request.TYPE_LOG:
                     //need to handle
                     //FileLogger.writeToFile(new String(payload.getPayload()));
                break;

                default:
                        break;
                }*/
               // return new StringBuffer();
        }





        /*
         * (non-Javadoc)
         * @see com.tad.framework.EventHandler.EventManager#handle(int, int, com.tad.framework.model.Response)
         */
        protected void handle(int action, int type, Response payload) {
        }
        /**
         *
         * @return
         */
        /*private HttpClient createHttpClient()
    	{
    		HttpParams params = new BasicHttpParams();
    		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    		HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
    		HttpProtocolParams.setUseExpectContinue(params, true);
    		SchemeRegistry schReg = new SchemeRegistry();
    		schReg.register(new Scheme("http",
    				PlainSocketFactory.getSocketFactory(), 80));
    		schReg.register(new Scheme("https",
    				SSLSocketFactory.getSocketFactory(), 443));
    		ClientConnectionManager conMgr = new
    		ThreadSafeClientConnManager(params,schReg);
    		return new DefaultHttpClient(conMgr, params);
    	}*/

}
