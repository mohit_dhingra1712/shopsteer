/**
 * Request.java
 * DbyDx Software ltd. @2011 - 2011+1
 * Confidential and proprietary.
 */
package com.tad.framework.model;

import java.util.HashMap;
import java.util.Hashtable;


/**
 */
public class Request
{
	public static final int TYPE_GET = 0;  //For Get Request
	public static final int TYPE_POST = 1; //For Post Request
	public static final int TYPE_MULTI_PART = 2; //For Post Request
	public static final int TYPE_LOG = 3;

	private String uri;
	private int requestId;
	private int requestType;
	private int timeoutSeconds;
	private HashMap properties;
	private int priority;
	private HashMap<String, String>  payloadString;
	private byte[] payload;

	public Request(){
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public int getRequestType() {
		return requestType;
	}
	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}
	public int getTimeoutSeconds() {
		return timeoutSeconds;
	}
	public void setTimeoutSeconds(int timeoutSeconds) {
		this.timeoutSeconds = timeoutSeconds;
	}
	public HashMap getProperties() {
		return properties;
	}
	public void setProperties(HashMap properties) {
		this.properties = properties;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public byte[] getPayload() {
		return payload;
	}
	public void setPayload(byte[] payload) {
		this.payload = payload;
	}
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public HashMap<String, String> getPayloadParams() {
		return payloadString;
	}

	public void setPayloadParams(HashMap<String, String> params ) {
		this.payloadString = params;
	}
}
