package permunite.com.shopsteer.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tad.framework.model.MyError;
import com.tad.framework.ui.BaseMainActivity;

import permunite.com.shopsteer.R;
import permunite.com.shopsteer.constants.EventType;
import permunite.com.shopsteer.controllers.CitylistController;
import permunite.com.shopsteer.controllers.StoreListController;
import permunite.com.shopsteer.model.ShoppingArea;

public class HomeActivity extends BaseMainActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final TextView mTextView = (TextView) findViewById(R.id.textsimple);
        Button storelist = (Button) findViewById(R.id.Storelist);
        storelist.setOnClickListener(this);


        CitylistController Mallnamecontroller = new CitylistController(HomeActivity.this, EventType.EVENT_TEST_VOLLY);
        Mallnamecontroller.requestService("citylist");


      /*  RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://www.google.com";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Display the first 500 characters of the response string.
                mTextView.setText("Response is: " + response.substring(0, 500));
            }
        }, new Response.ErrorListener() {
            @Override


            public void onErrorResponse(VolleyError error) {
            mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);*/

    }


    @Override
    public void setScreenData(final Object screenData, int event, long time) {
        super.setScreenData(screenData, event, time);
        if (screenData != null) {
            if (screenData instanceof MyError) {

                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HomeActivity.this, ((MyError) screenData).getErrorMsg() + "", Toast.LENGTH_SHORT).show();
                        //Parul
                        //Network and server issue handling must be added here only
                    }
                });
            } else {

                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HomeActivity.this, screenData.toString() + "", Toast.LENGTH_SHORT).show();
               /* String obj = screenData.toString();
                Message msg = new Message();
                msg.obj = obj;
                msg.what = event;
                handler.sendMessage(msg);*/

                    }
                });
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.Storelist :
                int MallId  = 17;
                //temp object creation to be replaced by actual object
                ShoppingArea mArea = new ShoppingArea();
                mArea.setMallID(17);
                StoreListController storelistcon = new StoreListController(this,EventType.EVENT_GET_STORELIST);
                storelistcon.requestService(mArea);
                    break;
        }
    }
}
