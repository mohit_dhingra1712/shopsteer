package permunite.com.shopsteer.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tad.framework.model.MyError;
import com.tad.framework.ui.BaseMainActivity;

import permunite.com.shopsteer.R;
import permunite.com.shopsteer.constants.EventType;
import permunite.com.shopsteer.controllers.CitylistController;
import permunite.com.shopsteer.dto.ResponseCityList;
import permunite.com.shopsteer.ui.AppConstants;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends BaseMainActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private int mSplashTime = 1000;
    private Thread mSplashThread;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;

    private boolean mVisible;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        mVisible = true;
        // mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);


        mSplashThread = new Thread() {
            @Override
            public void run() {

                //hardcode true for now,add soem condition to know this is not first Launch
                //Check if there is city or Location Saved in Preference
                if (false) {
                    synchronized (this) {
                        try {
                            wait(mSplashTime);
                            fetchCities();
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                         /*   Intent i = new Intent();
                            i.setClass(SplashActivity.this, Walkthrough.class);
                            startActivity(i);
                            finish();*/
                        }
                    }

                } else {//inacse app launch 2nd time when we have City and intital details with us
                    synchronized (this) {
                        try {
                            wait(mSplashTime);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        Log.d("Mohit", "inside splash");
                        Intent i = new Intent();
                        i.setClass(SplashActivity.this, HomeActivity.class);
                        startActivity(i);
                        SplashActivity.this.finish();

                    }
                }
            }
        };
        mSplashThread.start();

    }

    private void fetchCities() {
        CitylistController Citycontroller = new CitylistController(SplashActivity.this, EventType.EVENT_TEST_VOLLY);
        Citycontroller.requestService("citylist");
    }

    @Override
    public void setScreenData(final Object screenData, int event, long time) {
        if (screenData != null) {
            if (screenData instanceof MyError) {

                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SplashActivity.this, ((MyError) screenData).getErrorMsg() + "", Toast.LENGTH_SHORT).show();
                        // mLoadingTxt.setVisibility(View.GONE);
                        //Parul
                        //Network and server issue handling must be added here only

                    }
                });
            } else if (screenData instanceof ResponseCityList) {
                ResponseCityList obj = (ResponseCityList) screenData;
                Intent i = new Intent();
                i.setClass(SplashActivity.this, Walkthrough.class);
                i.putExtra(AppConstants.INTENT_CITY_LIST, obj.getResults());
                startActivity(i);
                SplashActivity.this.finish();

            }

        }
    }

}
