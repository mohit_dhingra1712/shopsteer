package permunite.com.shopsteer.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tad.framework.ui.BaseMainActivity;

import permunite.com.shopsteer.R;
import permunite.com.shopsteer.constants.EventType;
import permunite.com.shopsteer.controllers.SubmitUserDetails;
import permunite.com.shopsteer.model.LocationDetails;
import permunite.com.shopsteer.model.UserProfile;
import permunite.com.shopsteer.ui.AppConstants;

public class Walkthrough extends BaseMainActivity {

    LocationDetails mLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        initViews();
        if(RequestUserForLocation()){
            ActionLaunchHome();//Launch Home with Location Updates recieved from Play service
        }else
        {
            ShowCitylist();
        }




    }

    private void initViews()
    {
        Button btn = (Button)findViewById(R.id.butmn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }

    //Launch HomeActivty Passlocation Object
    private void ActionLaunchHome()
    {
        Intent i = new Intent();
        i.setClass(Walkthrough.this, HomeActivity.class);
        i.putExtra(AppConstants.Location_object,mLocation);
        startActivity(i);
        finish();
    }




    /*
    *Show Google Location request dialog
    *OnSucess: Get city name from Location Response
    *         Get lat n Long
    *         Update location Object
    *Go to HOME with lat n Long

    *OnDenied : Show City Drop Down
     */
    private boolean RequestUserForLocation()
    {
        return true;
    }

    /*
    Create City list from Intent or DB.
    OnCity Select : Go to Home with City name
     */
    private void ShowCitylist()
    {

        //onCityselected fill Location object and Launch Home

    }


    /*
    This method to be called if user opted for Login and we have saved user Profile Data
    This method should work in Background after Sucess response Update Prefrences as updated onServer
    Incase of Failure we need to retry certain times [TBD]
     */
    private void SubmitUserToServer()
    {
        SubmitUserDetails usercontroller =  new SubmitUserDetails(Walkthrough.this, EventType.EVENT_SUBMIT_USER);
        usercontroller.requestService(getuserObject());

    }

    private UserProfile getuserObject()
    {
        UserProfile user = new UserProfile();
        user.setUserName("karuna@gmail.com");
        user.setName("karuna gupta");
        user.setUserEmail("karuna@gmail.com");
        user.setGender("male");
        user.setUserId("24");
        return user;
    }
}
