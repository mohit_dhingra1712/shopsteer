package  permunite.com.shopsteer.ui;

	public interface AppConstants {
		
		/*************************Web Constants*********************/
	     public static String URL_TEST = "https://api.androidhive.info/volley/person_object.json";
		public static String URL_GET_CITYLIST= "http://meditroops.com/api/HospitalApi/GetCitiesByCountry?countryId=IND";
		public static String URL_SUBMIT_PROFILE = "http://www.meditroops.com/media/api/HomeApi/SaveUserProfile?";
		public static String URL_GET_STORELIST ="http://shopsteer.com/api/storelist";




		public static String INTENT_CITY_LIST = "CityList_intent" ;
	    
	    /*************************Other Constants*********************/
		public static String Location_object = "SavedLocation";
	    public static String Latitude = "latitude";
	    public static String Longitude = "longitude";
	    public static String Locality = "locality";
	    public static String Addressline = "Address";
	    public static String TERMOFUSE = "termofuse";
	    public static String CONTACTUS = "contactus";
	    public static String ABOUTUS = "aboutus";
	    public static String PRIVACYPOLICY = "privacypolicy";
	    
	    
	    public static String SOCIAL_ACCOUNT_TYPE = "social_account_type";
	    public static int ACCOUNT_TYPE_GOOGLE = 1000;
	    public static int ACCOUNT_TYPE_FACEBOOK = 1001;
	    public static int ACCOUNT_TYPE_MEDITROOPS = 1002;
	    
}
