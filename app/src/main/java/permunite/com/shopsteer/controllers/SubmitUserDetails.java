package permunite.com.shopsteer.controllers;

import android.app.Activity;
import android.text.TextUtils;


import com.tad.framework.EventHandler.AndroidEventManager;
import com.tad.framework.controller.BaseServiceController;
import com.tad.framework.model.MyError;
import com.tad.framework.model.Request;
import com.tad.framework.model.Response;
import com.tad.framework.ui.IActionController;

import permunite.com.shopsteer.constants.ActionType;
import permunite.com.shopsteer.constants.EventType;
import permunite.com.shopsteer.model.UserProfile;
import permunite.com.shopsteer.ui.AppConstants;

public class SubmitUserDetails extends BaseServiceController {
	private Activity _context;
	private UserProfile mProfile;
	public SubmitUserDetails(IActionController screen, int eventType) {
		super(screen, eventType);
		_context = (Activity) mScreen;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initService() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestService(final Object object) {{
		new Thread(new Runnable() {
			

			public void run() {
				//Handling the network unavailability..........
				/*if(!NativeHelper.isDataConnectionAvailable(_context)){
					responseService(new MyError("Network not available.", MyError.NETWORK_NOT_AVAILABLE,"ConnectionAvailable check of AppConfigController's requestService fn."));
					return;
				}*/
				//Making the Request event  for data....
				try{
					AndroidEventManager eventManager = new AndroidEventManager(_context) {
						public void handle(int action, int type, final Response payload) {
							unregister();
							responseService(payload);
						}
					};
					eventManager.register(ActionType.ACTION_SUBMIT_USER);
					//creating the post data Json....
					UserProfile profile = new UserProfile();
					profile = (UserProfile)object;
					
					 //Creating the request from Json......
					String req = profile.toJson();
					Request request = new Request();
					String url = AppConstants.URL_SUBMIT_PROFILE;
					//System.out.println(req);
					request.setUri(url);
					request.setPayload(req.getBytes());
					request.setRequestType(Request.TYPE_POST);
					//request.setProperties(properties);

					eventManager.raise(ActionType.ACTION_SUBMIT_USER, mEventType , request);
				}catch(Exception ex){
					responseService(new MyError(ex.getMessage(), MyError.EXCEPTION,"Making the Request to server in AppConfigController's requestService fn."));
				}
			}
		}).start();
	}
	}

	@Override
	public void responseService(Object object) {
		// TODO Auto-generated method stub
		if (object instanceof MyError) {
			mScreen.setScreenData(object, mEventType, 0);
			return;
		}
		if (object instanceof Response) {
			try {
				
				
				String responseStr = ((Response) object).getResponseText();
				if(mEventType == EventType.EVENT_SUBMIT_USER)
				{
					if (!TextUtils.isEmpty(responseStr)) {
						//ResponseHospital obj = new ResponseHospital();
						//obj.fromJson(responseStr);
						mScreen.setScreenData(responseStr, mEventType, 0);
				}
				}
			}
			 catch (Exception ex) {
					mScreen.setScreenData(
							new MyError(
									ex.getMessage(),
									MyError.EXCEPTION,
									"Handling the response from server in RHospitalController requestService fn."),
							mEventType, 0);
				}
		
		}else {
			mScreen.setScreenData("", mEventType, 0);
		}
	}
}
