package permunite.com.shopsteer.controllers;

import android.app.Activity;
import android.text.TextUtils;

import com.tad.framework.EventHandler.AndroidEventManager;
import com.tad.framework.controller.BaseServiceController;
import com.tad.framework.model.MyError;
import com.tad.framework.model.Request;
import com.tad.framework.model.Response;
import com.tad.framework.ui.IActionController;

import java.util.HashMap;

import permunite.com.shopsteer.constants.ActionType;
import permunite.com.shopsteer.constants.EventType;
import permunite.com.shopsteer.dto.ResponseCityList;
import permunite.com.shopsteer.dto.ResponseStoreList;
import permunite.com.shopsteer.model.ShoppingArea;
import permunite.com.shopsteer.ui.AppConstants;


/**
 * Created by mohit on 11/24/2017.
 */

public class StoreListController extends BaseServiceController {

    private Activity _context;
    private ShoppingArea mArea;

    public StoreListController(IActionController screen, int eventType) {
        super(screen, eventType);
        _context = (Activity) mScreen;
    }

    @Override
    public void initService() {

    }

    @Override
    public void requestService(Object object) {

              /*  if (!NativeHelper
                        .isDataConnectionAvailable((Activity) CitylistController.this.mScreen)) {
                    responseService(new MyError("Network not available.",
                            MyError.NETWORK_NOT_AVAILABLE,
                            "ConnectionAvailable check of RHospitalController requestService."));
                    return;
                }*/

        try {
            AndroidEventManager eventManager = new AndroidEventManager(
                    _context) {
                public void handle(int action, int type,
                                   final Response payload) {
                    unregister();
                    responseService(payload);
                }
            };

            if(mEventType == EventType.EVENT_GET_STORELIST)
            {
                eventManager.register(ActionType.ACTION_GET_STORELIST);
                //creating the post data Json....
                       /* Hospital hosp = new Hospital();
                        mNewHospital = (Hospital)object;*/

                //Creating the request from Json......
                // String req = mNewHospital.toJson();
                mArea = (ShoppingArea)object;
                Request request = new Request();
                HashMap<String, String> hashTable = new HashMap<String, String>();
                hashTable.put("mall", mArea.getMallID()+"");
                String url = AppConstants.URL_GET_STORELIST;
                //System.out.println(req);
                request.setUri(url);
                //request.setPayload(req.getBytes());
                request.setRequestType(Request.TYPE_GET);
                request.setProperties(hashTable);
                eventManager.raise(ActionType.ACTION_GET_STORELIST, mEventType , request);



            }                } catch (Exception ex) {
            responseService(null);
        }






    }

    @Override
    public void responseService(Object object) {

        if (object instanceof MyError) {
            mScreen.setScreenData(object, mEventType, 0);
            return;
        }
        if (object instanceof Response) {
            try {
                String responseStr = ((Response) object).getResponseText();
                if (!TextUtils.isEmpty(responseStr)) {
                    ResponseStoreList obj = new ResponseStoreList();
                    obj.fromJson(responseStr);
                    mScreen.setScreenData(obj, mEventType, 0);
                } else {
                    mScreen.setScreenData(
                            new MyError(
                                    "No Response from server",
                                    MyError.EXCEPTION,
                                    "Handling the response from server in RHospitalController requestService fn."),
                            mEventType, 0);
                }
            } catch (Exception ex) {
                mScreen.setScreenData(
                        new MyError(
                                ex.getMessage(),
                                MyError.EXCEPTION,
                                "Handling the response from server in RHospitalController requestService fn."),
                        mEventType, 0);
            }
        } else {
            mScreen.setScreenData("", mEventType, 0);
        }



    }
}
