package permunite.com.shopsteer.model;

import android.os.Parcel;
import android.os.Parcelable;

public class City implements Parcelable{
	private long CityID;
	public City() {
		// TODO Auto-generated constructor stub
	}

	public City(Parcel in) {
		// TODO Auto-generated constructor stub
		this.CityID = in.readLong();
		this.CityName = in.readString();
		this.CountryCode = in.readString();
		this.CountryName = in.readString();
		this.StateName = in.readString();
		this.StateID = in.readInt();
	}
	
	public City(String name) {
		this.CityName = name;
	}

	public long getCityID() {
		return CityID;
	}

	public void setCityID(long cityID) {
		CityID = cityID;
	}

	public int getStateID() {
		return StateID;
	}

	public void setStateID(int stateID) {
		StateID = stateID;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getCityName() {
		return CityName;
	}

	public void setCityName(String cityName) {
		CityName = cityName;
	}

	public String getStateName() {
		return StateName;
	}

	public void setStateName(String stateName) {
		StateName = stateName;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	private int StateID;
	private String CountryCode;
	private String CityName;
	private String StateName;
	private String CountryName;
	
	public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {

		@Override
		public City createFromParcel(Parcel arg0) {
			// TODO Auto-generated method stub
			return new City(arg0);
		}

		@Override
		public City[] newArray(int arg0) {
			// TODO Auto-generated method stub
			return new City[arg0];
		}

	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeLong(CityID);
		dest.writeString(CityName);
		dest.writeString(CountryCode);
		dest.writeString(CountryName);
		dest.writeString(StateName);
		dest.writeInt(StateID);
	}
	
	public String getCityNameById(int id) {
		if (this.CityID == id)
			return this.CityName;
		return "";
		
	}

}
