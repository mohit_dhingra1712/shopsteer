package permunite.com.shopsteer.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Store implements Parcelable{

	 private int StoreID;
	

	public int getStoreID() {
		return StoreID;
	}

	public void setStoreID(int storeID) {
		StoreID = storeID;
	}

	public int getMallID() {
		return MallID;
	}

	public void setMallID(int mallID) {
		MallID = mallID;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getTimings() {
		return Timings;
	}

	public void setTimings(String timings) {
		Timings = timings;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}

	public String getAbout() {
		return About;
	}

	public void setAbout(String about) {
		About = about;
	}

	public String getLatnLong() {
		return latnLong;
	}

	public void setLatnLong(String latnLong) {
		this.latnLong = latnLong;
	}

	public String getLogoURI() {
		return logoURI;
	}

	public void setLogoURI(String logoURI) {
		this.logoURI = logoURI;
	}

	public String getWebSite() {
		return WebSite;
	}

	public void setWebSite(String webSite) {
		WebSite = webSite;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getStoreImageUri() {
		return StoreImageUri;
	}

	public void setStoreImageUri(String storeImageUri) {
		StoreImageUri = storeImageUri;
	}

	private int MallID ;
	 private String Name;
	 private String Address;
	 private String Timings;
	 private String Telephone;
	 private String About;
	 private String latnLong;
	 private String logoURI ;
	 private String WebSite; 
	 private String Email ;
	 private String StoreImageUri ;
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(StoreID);
		dest.writeInt(MallID);
		dest.writeString(Name);
		dest.writeString(Address);
		dest.writeString(Timings);
		dest.writeString(Telephone);
		dest.writeString(About);
		dest.writeString(latnLong);
		dest.writeString(logoURI);
		dest.writeString(WebSite);
		dest.writeString(Email);
		dest.writeString(StoreImageUri);
		
		
	}
	 public Store(Parcel in) {
			// TODO Auto-generated constructor stub
		 this.StoreID = in.readInt();
		 this.MallID = in.readInt();
		 this.Name = in.readString();
		 this.Address = in.readString();
		 this.Timings = in.readString();
		 this.Telephone = in.readString();
		 this.About = in.readString();
		 this.latnLong = in.readString();
		 this.logoURI = in.readString();
		 this.WebSite = in.readString();
		 this.Email = in.readString();
		 this.StoreImageUri = in.readString();
		}

	public Store() {
		// TODO Auto-generated constructor stub
	}

	public static final Creator<Store> CREATOR = new Creator<Store>() {

		@Override
		public Store createFromParcel(Parcel arg0) {
			// TODO Auto-generated method stub
			return new Store(arg0);
		}

		@Override
		public Store[] newArray(int arg0) {
			// TODO Auto-generated method stub
			return new Store[arg0];
		}

	};

}
