package permunite.com.shopsteer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mohit on 12/6/2017.
 */

public class LocationDetails implements Parcelable{

    private double latitude;
    private double longitude;
    private String CityName;
    private String Areaname;
    private String State;


    protected LocationDetails(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
        CityName = in.readString();
        Areaname = in.readString();
        State = in.readString();
    }

    public static final Creator<LocationDetails> CREATOR = new Creator<LocationDetails>() {
        @Override
        public LocationDetails createFromParcel(Parcel in) {
            return new LocationDetails(in);
        }

        @Override
        public LocationDetails[] newArray(int size) {
            return new LocationDetails[size];
        }
    };

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getAreaname() {
        return Areaname;
    }

    public void setAreaname(String areaname) {
        Areaname = areaname;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(CityName);
        parcel.writeString(Areaname);
        parcel.writeString(State);
    }


}
