package permunite.com.shopsteer.model;

/**
 * Created by mohit on 12/1/2017.
 */

public class ShoppingArea {

    int MallID;
    String DestinationType;
    String MallName;

    String	MallAddress	;
    String	logoUri	;
    double	latitude	;

    public int getMallID() {
        return MallID;
    }

    public void setMallID(int mallID) {
        MallID = mallID;
    }

    public String getDestinationType() {
        return DestinationType;
    }

    public void setDestinationType(String destinationType) {
        DestinationType = destinationType;
    }

    public String getMallName() {
        return MallName;
    }

    public void setMallName(String mallName) {
        MallName = mallName;
    }

    public String getMallAddress() {
        return MallAddress;
    }

    public void setMallAddress(String mallAddress) {
        MallAddress = mallAddress;
    }

    public String getLogoUri() {
        return logoUri;
    }

    public void setLogoUri(String logoUri) {
        this.logoUri = logoUri;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public int getStoreCount() {
        return StoreCount;
    }

    public void setStoreCount(int storeCount) {
        StoreCount = storeCount;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public int getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(int postalCode) {
        PostalCode = postalCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getWebSite() {
        return WebSite;
    }

    public void setWebSite(String webSite) {
        WebSite = webSite;
    }

    public String getHowtoReach() {
        return HowtoReach;
    }

    public void setHowtoReach(String howtoReach) {
        HowtoReach = howtoReach;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getZone() {
        return Zone;
    }

    public void setZone(String zone) {
        Zone = zone;
    }

    double	Longitude	;
    int	StoreCount	;
    String	City	;
    String	State	;
    String	Country	;
    int	PostalCode	;
    String	Phone	;
    String	Email	;
    String	WebSite	;
    String	HowtoReach	;
    String	Floor	;
    String	Zone	;
}
