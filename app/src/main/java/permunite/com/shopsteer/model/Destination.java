package permunite.com.shopsteer.model;



import android.os.Parcel;
import android.os.Parcelable;

public class Destination implements Parcelable{
	
	private int MallID ;
	private String DestinationType;
	private String MallName ;
	private String MallAddress;
	private String logoUri ;
	private String latitude ;
	private String Longitude ;
	private int StoreCount ;
	private String City;
	private String State ;
	private String Country ;
	private String PostalCode ;
	private String Phone;
	private String Email ;
	private String WebSite;
	private String HowtoReach ;
	
	public Destination()
	{
		
	}
	public int getMallID() {
		return MallID;
	}
	public void setMallID(int mallID) {
		MallID = mallID;
	}
	public String getDestinationType() {
		return DestinationType;
	}
	public void setDestinationType(String destinationType) {
		DestinationType = destinationType;
	}
	public String getMallName() {
		return MallName;
	}
	public void setMallName(String mallName) {
		MallName = mallName;
	}
	public String getMallAddress() {
		return MallAddress;
	}
	public void setMallAddress(String mallAddress) {
		MallAddress = mallAddress;
	}
	public String getLogoUri() {
		return logoUri;
	}
	public void setLogoUri(String logoUri) {
		this.logoUri = logoUri;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public int getStoreCount() {
		return StoreCount;
	}
	public void setStoreCount(int storeCount) {
		StoreCount = storeCount;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getWebSite() {
		return WebSite;
	}
	public void setWebSite(String webSite) {
		WebSite = webSite;
	}
	public String getHowtoReach() {
		return HowtoReach;
	}
	public void setHowtoReach(String howtoReach) {
		HowtoReach = howtoReach;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(MallID);
		dest.writeString(MallName);
		dest.writeInt(StoreCount);
		dest.writeString(MallAddress);
		dest.writeString(PostalCode);
		dest.writeString(City);
		dest.writeString(State);
		dest.writeString(Country);
		dest.writeString(Longitude);
		dest.writeString(latitude);
		dest.writeString(Email);
		dest.writeString(Phone);
		dest.writeString(logoUri);
		dest.writeString(WebSite);
		dest.writeString(HowtoReach);
		dest.writeString(DestinationType);
		
		
	}
	
	

	private Destination(Parcel in) {
		this.MallID = in.readInt();
		this.MallName = in.readString();
		this.StoreCount = in.readInt();
		this.MallAddress = in.readString();
		this.PostalCode = in.readString();
		this.City = in.readString();
		this.State = in.readString();
		this.Country = in.readString();
		this.Longitude = in.readString();
		this.latitude = in.readString();
		this.Email = in.readString();
		this.Phone = in.readString();
		this.logoUri = in.readString();
		this.WebSite = in.readString();
		this.HowtoReach = in.readString();
		this.DestinationType = in.readString();
		
		
	}

	public static final Creator<Destination> CREATOR = new Creator<Destination>() {

		@Override
		public Destination createFromParcel(Parcel arg0) {
			// TODO Auto-generated method stub
			return new Destination(arg0);
		}

		@Override
		public Destination[] newArray(int arg0) {
			// TODO Auto-generated method stub
			return new Destination[arg0];
		}

	};

	
	
}
