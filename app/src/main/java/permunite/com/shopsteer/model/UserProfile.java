package permunite.com.shopsteer.model;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserProfile {
	
	
	private String UserName;
	private String Name;
	private String UserEmail;
	private String UserDOB;
	private String UserId ;
	private String Gender ;
	private String Provider= "";
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserEmail() {
		return UserEmail;
	}
	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}
	public String getUserDOB() {
		return UserDOB;
	}
	public void setUserDOB(String userDOB) {
		UserDOB = userDOB;
	}
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String toJson() {

		Gson gson = new Gson();
		String userJson = gson.toJson(this);

/*
		Gson gson = new Gson();
		Type stringStringMap = new TypeToken<Map<String, String>>(){}.getType();
		Map<String,String> map = gson.fromJson(json, stringStringMap);*/

/*


		String JsonString = "";
		//UserPolicyDetail obj = new UserPolicyDetail();
		
		try {
			JSONObject jsonobject = new JSONObject();
			//jsonobject.put("UserId", 1);
			jsonobject.put("UserName", UserEmail);
			jsonobject.put("Name", UserName);
			jsonobject.put("Email", UserEmail);
			jsonobject.put("Gender", "Male");
			jsonobject.put("Mode", "Mobile");
			jsonobject.put("Provider", "");
			//jsonobject.put("Policies", PDetails.toJson());

			
			
						
			JsonString = jsonobject.toString();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		return userJson;

	}

}
