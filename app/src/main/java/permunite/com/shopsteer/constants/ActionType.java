package permunite.com.shopsteer.constants;

public interface ActionType {
	public static final int ACTION_TEST_VOLLY=0;
	public static final int ACTION_SUBMIT_USER=1;
	public static final int ACTION_GET_STORELIST=2;
	public static final int ACTION_GET_CITYLIST=3;
	public static final int ACTION_GET_DESTINATIONLIST=4;
	public static final int ACTION_GET_STOREDETAILS=5;
	public static final int ACTION_GET_USERDETAILS=6;

}
