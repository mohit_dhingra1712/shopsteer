package permunite.com.shopsteer.constants;

public interface EventType {
	public static final int EVENT_TEST_VOLLY = 0;
	public static final int EVENT_SUBMIT_USER = 1;
	public static final int EVENT_GET_STORELIST = 2;
	public static final int EVENT_GET_CITYLIST = 3;

}
