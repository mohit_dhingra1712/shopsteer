package permunite.com.shopsteer.dto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

import permunite.com.shopsteer.model.City;
import permunite.com.shopsteer.model.Store;

/**
 * Created by mohit on 11/30/2017.
 */

public class ResponseStoreList {

    private ArrayList<Store> mData;

    public ArrayList<Store> getResults() {
        return mData;
    }

    public ResponseStoreList fromJson(String json) {

        Gson gson = new Gson();
        //Type founderListType = new TypeToken<ArrayList<Store>>() { }.getType();
        Type collectionType = new TypeToken<ArrayList<Store>>(){}.getType();
        mData = gson.fromJson(json, collectionType);
        return this;
    }
}
