package permunite.com.shopsteer.dto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import permunite.com.shopsteer.model.City;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ResponseCityList {
    private ArrayList<City> mData;

    public ArrayList<City> getResults() {
        return mData;
    }

    public ResponseCityList fromJson(String json) {

        Gson gson = new Gson();
        Type founderListType = new TypeToken<ArrayList<City>>() { }.getType();
        mData = gson.fromJson(json, founderListType);
        return this;
    }
}
